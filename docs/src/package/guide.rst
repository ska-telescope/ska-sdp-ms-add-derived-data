.. doctest-skip-all
.. _package-guide:

.. todo::
    - add the antenna table to the measurement set
    - perhaps add arbitrary measurement set tables based on the contents of a JSON file
    - add the information about the JSON table format

****************************************
MSADD - Measurement Set Add Derived Data
****************************************

Simple tool to add derived products to existing measurement sets. Basic functionality is
to add UVW in a catalog frame to a measurement set - based on the DELAY_DIR direction and the
antenna positions. But it is envisioned that other tables will be added.

UVW Calculation and Addition
============================

The scheme to perform this is based upon the CASACORE package and utilises the python-casacore wrappers
to the measures functionality of those classes. There are other member functions of this package that
do not utilise this functionality.
.. Automatic API Documentation section. Generating the API from the docstrings. Modify / change the directory structure as you like

The Applications
````````````````

msadd-uvw
---------


The first application developed with this package is ``msadd-uvw`` it can be run from the command line. Options can be
examined by ``msadd-uvw -h``

Essentially this takes a direction, a set of antenna positions and updates an output measurement set with the UVW calculated
by the utilities in the package.

The following options are supported:

- ``-i, --input``: MSv2 input model file used for reference antenna positions, the ``locations`` option should really be used. (default: None)

- ``-o, --output``: Output MSv2 - this is the table which needs updating. (default: None)

- ``-l, --locations`` : JSON input antenna-locations file - this can be a local location or a URL (default: None)

- ``-d, --delay``: Delay Centre Over-ride -- You should set this. Otherwise the tools will inspect the output measurement set for the FIELD table. The units are (hour, degree). The format is anything an Astropy SkyCoord will take so \"00 42 30 +41 12 00" or \"00:42.5 +41:12" are fine

- ``f, --frame``: Reference frame for the delay direction (ICRS)

- ``s, --swap``: Default order of baseline calculation is Antenna2 - Antenna1 - you can swap this if you want.


The Module Contents
```````````````````

The Antenna Table
-----------------

A simple set of classes and functions to read and construct an antenna
table in memory from either a reference measurement set of a JSON file.
THe JSON file can be stored remotely and accessed via URL

.. autoclass:: ska.sdp.msadd.antenna.AntennaTable
    :members:



The UVW Table
-------------

These methods are used to calculate UVW based upon the contents of a telescope
model (antenna locations, Time, look direction). The contents of which are
obtained from the AntennaTable and a reference measurement set.

.. autoclass:: ska.sdp.msadd.uvw.UpdateUvwColumn
    :members:

The Calculation Utilities
-------------------------

The actual work in calculating the UVW is performed by these functions. There are some utilities that perform these
tasks from first principles and others that use casacore measures - as accessed via the python wrappers

.. automodule:: ska.sdp.msadd.utils
    :members:
