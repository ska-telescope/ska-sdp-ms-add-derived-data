#!/usr/bin/env python3
"""
    msadd-uvw -- An Application to add derived UVW parameters to a measurement set.
    It employs the methods in this package to update the UVW in a measurement set.
    The Antenna Positions can either come from a JSON formatted input file or a reference
    measurement set.

    """
import sys
from ska.sdp.msadd.uvw import UpdateUvwColumn
import argparse

import logging
import ska_ser_logging
from astropy.coordinates import SkyCoord
from astropy import units as u

logger = logging.getLogger(__name__)


def parse_arguments():

    default_input = None
    default_output = None
    default_locations = None
    default_delay_pointing = None


    parser = argparse.ArgumentParser(
        usage='msadd [options]',
        description=__doc__)

    parser.add_argument('-i', '--input',
                        help='MSv2 input model file used for reference antenna positions, a json location '
                             ' should really be used. (default: None)'
                        .format(default_input),
                        default=default_input)

    parser.add_argument('-o', '--output',
                        help='Output MSv2 - this is the table which needs updating. (default: None)'
                        .format(default_output),
                        default=default_output)

    parser.add_argument('-l', '--locations',
                        help='JSON input antenna-locations file - this can be a local location or a URL '
                             '(default: None)'
                        .format(default_locations),
                        default=default_locations)

    parser.add_argument('-d', '--delay',
                        help='Delay Centre Over-ride -- You should set this. Otherwise the tools will inspect the '
                             'output measurement set for the FIELD table. Hour angle for RA and degree for Dec is '
                             'assumed. The format is anything an Astropy SkyCoord will take so '
                             '\"00 42 30 +41 12 00\" or \"00:42.5 +41:12\" are fine',
                        default=default_delay_pointing)

    parser.add_argument('-f', '--frame',
                        help='Reference frame for the delay direction probably ICRS but you can pick any of the'
                             'SkyCoord supported frames (ICRS)',
                        default='icrs')

    parser.add_argument('-s', '--swap',
                        help='Default order of baseline calculation is Antenna2 - Antenna1 - you can swap this if you '
                             'want',
                        default=False)

    parser.add_argument(
        "-v",
        "--verbose",
        help="If set, more verbose output will be produced",
    )
    return parser


def main(arguments):

    delay_ra = None
    delay_dec = None

    parser = parse_arguments()

    if len(arguments) == 0:
        arguments = ["-h"]
        parser.parse_args(arguments)

    args = parser.parse_args(arguments)
    logging_level = logging.DEBUG if args.verbose else logging.INFO
    ska_ser_logging.configure_logging(level=logging_level)

    if args.delay:
        delay_dir = SkyCoord(args.delay, unit=(u.hourangle, u.deg), frame=args.frame)
        delay_ra = delay_dir.ra.radian
        delay_dec = delay_dir.dec.radian

    updater = UpdateUvwColumn(input_table_name=args.input,
                              json_file_location=args.locations,
                              swap_baselines=args.swap,
                              output_table_name=args.output,
                              delay_ra=delay_ra,
                              delay_dec=delay_dec)

    updater.update_uvw_for_all_rows()

def entry_point():
    main(sys.argv[1:])

if __name__ == '__main__':
    main(sys.argv[1:])
