###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

0.1.2
*****

Prototype version used in the demostration. No changes to functionality but
cleaned up Dockerfile and documentation


0.1.0
*****

Added
-----

* Initial release
* Can update a measurement set table
* Application can run standalone
* Can read antenna locations from MS. Local file or URL
* Basic UVW calculation
* Added the CSIRO licence

Notable absences:


* Does not update the antenna table
* Does not update the FIELD/POINTING table. This is likely to be needed. There are probably a number of missing
  tables that the receiver cannot add without the measurement set model. This needs to be further investigated and
  the functionality added.