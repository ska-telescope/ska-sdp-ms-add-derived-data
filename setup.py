#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup
import os

setup_dir = os.path.dirname(os.path.abspath(__file__))
release_filename = os.path.join(setup_dir, "src", "ska", "sdp", "msadd", "release.py")
exec(open(release_filename).read())


with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='ska_sdp_msadd',
    long_description=readme + '\n\n',
    author=author,
    author_email=author_email,
    url=url,
    version=version,
    packages=find_packages(exclude=["tests"]),
    package_dir={"": "src"},
    include_package_data=True,
    entry_points={
        'console_scripts': [ 'msadd-uvw=ska.sdp.msadd.msadd_uvw:entry_point' ]
    },
    license="BSD license",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    test_suite='tests',
    install_requires=[
        "numpy",
        "astropy",
        "python-casacore >= 3.1.1",
        "ska_ser_logging"
    ],
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'docutils >= 0.17, < 0.18',
        'sphinx >= 4.2.0',
        'sphinx-argparse',
        'recommonmark'
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pylint',
        'pytest-json-report',
        'pycodestyle',
    ],
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort'],
    }
)
